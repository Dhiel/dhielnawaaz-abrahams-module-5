import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:saphcafire/main.dart';

import 'List_firefoxgeneredlist.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      options: FirebaseOptions(
          apiKey: "AIzaSyDPIBoJaKHbvUA6xdG-UC3Pq9FBpUENZao",
          authDomain: "saphcapareg.firebaseapp.com",
          projectId: "saphcapareg",
          storageBucket: "saphcapareg.appspot.com",
          messagingSenderId: "2683960553",
          appId: "1:2683960553:web:6fe7174e0defb51b390478"));
  runApp(Regpro());
}

class Fire2 extends StatelessWidget {
  final Future<FirebaseApp> _initialization = Firebase.initializeApp();
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _initialization,
      builder: (context, snapshot) {
        // CHeck for Errors
        if (snapshot.hasError) {
          print("Something went Wrong");
        }
        // once Completed, show your application
        if (snapshot.connectionState == ConnectionState.done) {
          return MaterialApp(
            title: 'Flutter Firestore CRUD',
            theme: ThemeData(
              primarySwatch: Colors.blue,
            ),
            debugShowCheckedModeBanner: false,
            home: ListPage(),
          );
        }
        return CircularProgressIndicator();
      },
    );
  }
}

class Regpro extends StatelessWidget {
  Regpro({Key? key}) : super(key: key);

  final _formKey = GlobalKey<FormState>();

  var name = "name";
  var surname = "surname";
  var idnumber = "idnumber";
  var number = "contact number";
  var add = "address";
  var card = "patient card number";
  var password2 = "password";

  final nameController = TextEditingController();
  final surController = TextEditingController();
  final idController = TextEditingController();
  final numberController = TextEditingController();
  final addController = TextEditingController();
  final cardController = TextEditingController();
  final password2Controller = TextEditingController();

  CollectionReference students =
      FirebaseFirestore.instance.collection('Patient Details');

  static get projectId => saphcapareg;

  static get apiKey => null;

  static get authDomain => null;

  static get storageBucket => null;

  static get messagingSenderId => null;

  static get appId => null;

  static get saphcapareg => null;

  Future<void> addUser() {
    return students
        .add({
          'name': name,
          'surname': surname,
          'ID number': idnumber,
          'contact number': number,
          'address': add,
          'patient card number': card,
          'password': password2
        })
        .then((value) => print('Patient Details Added'))
        .catchError((error) => print('Failed to Add Patient Details: $error'));
  }

  static const String _title = 'Create a health profile';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      theme: ThemeData(
          colorScheme: ColorScheme.fromSwatch(
            primarySwatch: Colors.blue,
          ).copyWith(
            secondary: Colors.red,
          ),
          floatingActionButtonTheme: FloatingActionButtonThemeData(
            focusColor: Colors.green,
            hoverColor: Colors.black,
            splashColor: Colors.orange,
            backgroundColor: Colors.white,
            foregroundColor: Colors.blue,
          ),
          primaryColor: Colors.purple,
          errorColor: Color.fromRGBO(150, 0, 15, 1),
          backgroundColor: Colors.blueGrey,
          scaffoldBackgroundColor: Colors.white,
          textTheme: TextTheme(
              headline1: TextStyle(
            fontSize: 75,
            fontWeight: FontWeight.w600,
            color: Colors.white,
          ))),
      home: Scaffold(
        appBar: AppBar(title: const Text(_title)),
        body: const MyStatefulWidget(),
      ),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  TextEditingController nameController = TextEditingController();
  TextEditingController surController = TextEditingController();
  TextEditingController idController = TextEditingController();
  TextEditingController numberController = TextEditingController();
  TextEditingController addController = TextEditingController();
  TextEditingController cardController = TextEditingController();
  TextEditingController password2Controller = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(10),
        child: ListView(
          children: <Widget>[
            Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.all(10),
                child: const Text(
                  'New patient information',
                  style: TextStyle(
                      color: Color.fromARGB(255, 32, 78, 116),
                      fontWeight: FontWeight.w500,
                      fontSize: 30),
                )),
            Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.all(10),
                child: const Text(
                  'Complete the form below to create your account',
                  style: TextStyle(fontSize: 20),
                )),
            Container(
              padding: const EdgeInsets.all(10),
              child: TextField(
                controller: nameController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Patient Name',
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10),
              child: TextField(
                controller: surController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Patient Surname',
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10),
              child: TextField(
                controller: idController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Patient S.A identity number',
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10),
              child: TextField(
                controller: cardController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Patient clinic card number',
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10),
              child: TextField(
                controller: addController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Patient Address',
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10),
              child: TextField(
                controller: numberController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Patient contact number',
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
              child: TextField(
                obscureText: true,
                controller: passwordController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Password',
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
              child: TextField(
                obscureText: true,
                controller: password2Controller,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: ' Verify Password',
                ),
              ),
            ),
            TextButton(
              onPressed: () {},
              child: const Text(
                'Welcome to your first step towards better health',
              ),
            ),
            Container(
                height: 50,
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: ElevatedButton(
                  child: const Text('Submit to Register Your Health Account'),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => const login()),
                    );
                    print(nameController.text);
                    print(passwordController.text);
                  },
                )),
          ],
        ));
  }
}
